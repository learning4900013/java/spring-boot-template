# Spring Boot Template Project
This is a template project based on Spring Boot 3 that can be used as a base for other projects used for learning 
purposes. The project uses Java 17 as the programming language and H2 database with Hibernate 6 as the database 
management system.

## Getting Started
To get started with this project, follow these steps:

1. Clone the project from the repository:

    <code>git clone <repository_url></code>

2. Import the project into your favorite IDE as a Maven project.

3. Build the project:

    <code>mvn clean install</code>

4. Run the project:

    <code>mvn spring-boot:run</code>

This will start the Spring Boot application on http://localhost:8080.

## Database Configuration
The project uses H2 database with Hibernate 6 as the ORM. The database configuration can be found in the **application.properties** file.

## License
This project is licensed under the MIT License. You can find a copy of the license in the LICENSE file.

##  Acknowledgments
This project is based on the Spring Boot Getting Started Guide.





