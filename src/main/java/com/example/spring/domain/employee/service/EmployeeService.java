package com.example.spring.domain.employee.service;

import com.example.spring.domain.employee.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeService {
    Employee save(Employee employee);

    Employee getById(Long id);

    Page<Employee> get(Pageable pageable);

    Employee update(Long id, Employee employee);

    void delete(Long id);
}
