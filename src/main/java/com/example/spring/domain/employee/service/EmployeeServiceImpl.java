package com.example.spring.domain.employee.service;

import com.example.spring.domain.employee.entity.Employee;
import com.example.spring.domain.employee.repository.EmployeeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public Employee getById(Long id) {
        return findById(id);
    }

    @Override
    public Page<Employee> get(Pageable pageable) {
        return employeeRepository.findAll(pageable);
    }

    @Override
    public Employee update(Long id, Employee employee) {
        var dbEmployee = findById(id);
        dbEmployee.setFullName(employee.getFullName());
        dbEmployee.setEmail(employee.getEmail());
        dbEmployee.setPassword(employee.getPassword());
        dbEmployee.setAge(employee.getAge());

        return employeeRepository.save(dbEmployee);
    }

    @Override
    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }

    private Employee findById(Long id) {
        return employeeRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Employee with id " + id + " does not exist"));
    }
}
